package RezonitTesting.RezonitPages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class RezonitHomePage {
    SelenideElement printedCircuitBoards = Selenide.$x("//header/div[2]/div/div/ul/li[1]");

    public void openRezonitHomePage(){
        Selenide.open("https://www.rezonit.ru/");
    }

    public void printedCircuitBoarsClick() {
        printedCircuitBoards.click();
    }
}
