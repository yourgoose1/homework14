package RezonitTesting.RezonitPages;

public interface PageObjectSupplier {
    default RezonitHomePage rezonitHomePage(){
        return new RezonitHomePage();
    }
    default PrintedCircuitBoardsPage printedCircuitBoardsPage(){
        return new PrintedCircuitBoardsPage();
    }
}
