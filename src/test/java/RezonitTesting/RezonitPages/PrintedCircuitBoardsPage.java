package RezonitTesting.RezonitPages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class PrintedCircuitBoardsPage {
    SelenideElement inputLength = Selenide.$x("//input[@id='calculator-input-1']");
    SelenideElement inputWidth = Selenide.$x("//input[@id='calculator-input-2']");
    SelenideElement inputQuantity = Selenide.$x("//input[@id='calculator-input-3']");
    SelenideElement submitBtn = Selenide.$x("//button[@id='calculate']");
    SelenideElement totalPrice = Selenide.$x("//span[@id='total-price']");
    SelenideElement alert = Selenide.$x("//div[@class='alert alert-danger']");

    public void setLength(String length){
        inputLength.sendKeys(length);
    }

    public void setWidth(String width){
        inputWidth.sendKeys(width);
    }

    public void setQuantity(String quantity){
        inputQuantity.sendKeys(quantity);
    }

    public void clickSubmitBtn(){
        submitBtn.click();
    }

    public void checkTotalPrice(){
        totalPrice.should(Condition.visible);
    }

    public void checkAlert(){
        alert.should(Condition.visible);
    }

}
