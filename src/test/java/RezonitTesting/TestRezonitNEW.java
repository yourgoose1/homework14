package RezonitTesting;

import RezonitTesting.RezonitPages.PageObjectSupplier;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;

public class TestRezonitNEW implements PageObjectSupplier {
    @Test
    public void rezonitPositiveTest() {
        rezonitHomePage().openRezonitHomePage();
        rezonitHomePage().printedCircuitBoarsClick();
        printedCircuitBoardsPage().setLength("10");
        printedCircuitBoardsPage().setWidth("20");
        printedCircuitBoardsPage().setQuantity("20");
        printedCircuitBoardsPage().clickSubmitBtn();
        Selenide.sleep(5000);
        printedCircuitBoardsPage().checkTotalPrice();
    }

    @Test
    public void rezonitNegativeTest(){
        rezonitHomePage().openRezonitHomePage();
        rezonitHomePage().printedCircuitBoarsClick();
        printedCircuitBoardsPage().setLength("10");
        printedCircuitBoardsPage().setWidth("-999");
        printedCircuitBoardsPage().checkAlert();
    }
}
