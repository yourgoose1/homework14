package LitresTesting;

import LitresTesting.LitresPages.PageObjectSupplier;
import org.testng.annotations.Test;

public class TestLitresNEW implements PageObjectSupplier {
    @Test
    public void testLitres() {

        litresHomePage().openLitresHomePage();
        litresHomePage().searchBarInput("грокаем алгоритмы");
        litresHomePage().searchBtnClick();
        litresSearchingResultPage().firstElementClick();
        litresProductPage().checkPrice();
        litresProductPage().checkAddToBuyBtn();

    }

}
