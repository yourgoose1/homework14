package LitresTesting.LitresPages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class LitresHomePage {
    SelenideElement searchBar = Selenide.$x("//input[@data-test-id='header__search-input--desktop']");
    SelenideElement searchBtn = Selenide.$x("//button[@data-test-id='header__search-button--desktop']");


    public void openLitresHomePage(){
        Selenide.open("https://www.litres.ru/");
    }
    public void searchBarInput(String userEnter) {
        searchBar.sendKeys(userEnter);
    }

    public void searchBtnClick() {
        searchBtn.click();
    }
}
