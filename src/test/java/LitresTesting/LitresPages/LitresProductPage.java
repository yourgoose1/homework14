package LitresTesting.LitresPages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class LitresProductPage {
    SelenideElement price = Selenide.$x("//strong[contains(text(), '799')]");
    SelenideElement addToBuy = Selenide.$x("//div[contains(text(), 'Добавить в корзину')]");

    public void checkPrice(){
        price.should(Condition.visible);
    }
    public void checkAddToBuyBtn(){
        addToBuy.should((Condition.visible));
    }
}
