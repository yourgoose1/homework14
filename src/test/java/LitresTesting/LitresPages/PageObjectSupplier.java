package LitresTesting.LitresPages;

public interface PageObjectSupplier {
    default LitresHomePage litresHomePage(){
        return new LitresHomePage();
    }
    default LitresProductPage litresProductPage(){
        return new LitresProductPage();
    }
    default LitresSearchingResultPage litresSearchingResultPage(){
        return new LitresSearchingResultPage();
    }

}
